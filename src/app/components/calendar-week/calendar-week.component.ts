import { Component, OnInit, Input } from '@angular/core';
import { CalendarDate } from 'src/app/models/calendar-date.interface';

@Component({
  selector: 'calendar-week',
  templateUrl: './calendar-week.component.html',
  styleUrls: ['./calendar-week.component.scss']
})
export class CalendarWeekComponent implements OnInit {
  @Input() daysOfWeek: Array<CalendarDate>;

  constructor() { }

  ngOnInit() {
  }
}
