import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { CalendarDate, CalendarDateImpl } from 'src/app/models/calendar-date.interface';
import { resetTime } from 'src/app/utils';
import { CalendarState } from 'src/app/store/reducers';
import { Store, select } from '@ngrx/store';
import { choosenDate } from 'src/app/store/selectors';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  selected: moment.Moment;
  start: moment.Moment;
  weeks: Array<any>;
  today: moment.Moment;

  constructor(private store: Store<CalendarState>) { }

  ngOnInit() {
    this.today = resetTime(moment());
    this.store.pipe(select(choosenDate)).subscribe(date => this.buildCalendar(date));
  }

  private buildCalendar(selectedDate: moment.Moment | Date) {
    if (selectedDate instanceof Date) {
      selectedDate = moment(selectedDate);
    }
    this.selected = resetTime(selectedDate);
    this.start = this.selected.clone().date(1).day(0);
    this.buildMonth(this.start, this.selected.month());
  }

  private buildMonth(start: moment.Moment, month: number) {
    this.weeks = new Array<any>();
    const beginingWeek = start.clone();
    do {
      this.weeks.push(this.buildWeek(beginingWeek, month));
      beginingWeek.add(1, 'w');
    } while (beginingWeek.month() === month);
  }

  private buildWeek(start: moment.Moment, month: number) {
    const days = new Array<CalendarDate>();
    const day = start.clone();
    for (let i = 0; i < 7; i++) {
      days.push(new CalendarDateImpl(day.format(environment.shortDateFormat),
        this.isSelectedDate(day), this.isToday(day), this.isSameMonth(day, month)));
      day.add(1, 'd');
    }
    return days;
  }

  private isSelectedDate(date: moment.Moment): boolean {
    return this.selected.diff(date) === 0;
  }

  private isToday(date: moment.Moment): boolean {
    return this.today.diff(date) === 0;
  }

  private isSameMonth(date: moment.Moment, month: number): boolean {
    return date.month() === month;
  }
}
