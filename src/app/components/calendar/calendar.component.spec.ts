import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarComponent } from './calendar.component';
import * as moment from 'moment';
import { of, Subject } from 'rxjs';
import { Store, Action } from '@ngrx/store';
import { CalendarState } from 'src/app/store/reducers';
import { storeMockup } from 'src/app/store/store-mockup';

describe('CalendarComponent', () => {
  let component: CalendarComponent;
  let fixture: ComponentFixture<CalendarComponent>;

  beforeEach(async(() => {
    const actions = new Subject<Action>();
    const states = new Subject<CalendarState>();
    const store = storeMockup<CalendarState>({ actions, states });


    TestBed.configureTestingModule({
      declarations: [CalendarComponent],
      providers: [{
        provide: Store,
        useValue: store
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
