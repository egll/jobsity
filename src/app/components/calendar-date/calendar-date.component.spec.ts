import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarDateComponent } from './calendar-date.component';
import { Store, Action } from '@ngrx/store';
import { of, Subject } from 'rxjs';
import { storeMockup } from 'src/app/store/store-mockup';
import { CalendarState } from 'src/app/store/reducers';
import { CalendarDateImpl } from 'src/app/models/calendar-date.interface';

describe('CalendarDateComponent', () => {
  let component: CalendarDateComponent;
  let fixture: ComponentFixture<CalendarDateComponent>;

  beforeEach(async(() => {
    const actions = new Subject<Action>();
    const states = new Subject<CalendarState>();
    const store = storeMockup<CalendarState>({ actions, states });

    TestBed.configureTestingModule({
      declarations: [CalendarDateComponent],
      providers: [{ provide: Store, useValue: store }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarDateComponent);
    component = fixture.componentInstance;
    component.calendarDate = new CalendarDateImpl('2020-01-01');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
