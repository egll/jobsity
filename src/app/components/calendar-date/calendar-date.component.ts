import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CalendarDate, CalendarDateImpl } from 'src/app/models/calendar-date.interface';
import * as moment from 'moment';
import { CalendarState } from 'src/app/store/reducers';
import { Store } from '@ngrx/store';
import { SetSelectedDate } from 'src/app/store/actions';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'calendar-date',
  templateUrl: './calendar-date.component.html',
  styleUrls: ['./calendar-date.component.scss']
})
export class CalendarDateComponent implements OnInit {
  @Input() calendarDate: CalendarDate;
  dateTime: moment.Moment;
  isWeekend: boolean;

  constructor(private store: Store<CalendarState>) { }

  ngOnInit() {
    if (!this.calendarDate) {
      this.calendarDate = new CalendarDateImpl(moment().format(environment.shortDateFormat));
    }
    this.dateTime = moment(this.calendarDate.date, environment.shortDateFormat);
    this.isWeekend = this.dateTime.day() === 0 || this.dateTime.day() === 6;
  }

  emitSelection() {
    this.store.dispatch(new SetSelectedDate(this.dateTime));
  }
}
