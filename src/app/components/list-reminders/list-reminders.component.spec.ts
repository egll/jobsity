import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRemindersComponent } from './list-reminders.component';
import { Subject } from 'rxjs';
import { CalendarState } from 'src/app/store/reducers';
import { storeMockup } from 'src/app/store/store-mockup';
import { Action, Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';

describe('ListRemindersComponent', () => {
  let component: ListRemindersComponent;
  let fixture: ComponentFixture<ListRemindersComponent>;

  beforeEach(async(() => {
    const actions = new Subject<Action>();
    const states = new Subject<CalendarState>();
    const store = storeMockup<CalendarState>({ actions, states });

    TestBed.configureTestingModule({
      declarations: [ListRemindersComponent],
      providers: [{ provide: Store, useValue: store }, FormBuilder]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRemindersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
