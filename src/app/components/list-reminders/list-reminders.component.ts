import { Component, OnInit } from '@angular/core';
import { CalendarState } from 'src/app/store/reducers';
import { Store, select } from '@ngrx/store';
import { choosenDate, getReminders } from 'src/app/store/selectors';
import * as moment from 'moment';
import { Reminder } from 'src/app/models/reminder.interface';
import { faPlus, faTimes, faCheck, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AddReminder } from 'src/app/store/actions';
import { RemindersService } from 'src/app/services/reminders.service';
import { CitiesService } from 'src/app/services/cities.service';
import { WeatherService } from 'src/app/services/weather.service';
import { get } from 'lodash';
import { Subscription } from 'rxjs';

@Component({
  selector: 'list-reminders',
  templateUrl: './list-reminders.component.html',
  styleUrls: ['./list-reminders.component.scss']
})
export class ListRemindersComponent implements OnInit {
  date: moment.Moment;
  reminders: Array<Reminder>;
  weatherIcons: Array<any>;
  daysNames: Array<string>;
  newReminderIcon: IconDefinition;
  cancelReminderIcon: IconDefinition;
  saveReminderIcon: IconDefinition;

  status: any;

  cityFieldValue: string;
  initialColor: string;
  color: string;

  placesOptions: any;

  form = this.fb.group({
    title: ['', Validators.maxLength(30)],
    time: ['', Validators.required],
    city: [''],
    country: [''],
    color: ['', Validators.required]
  });

  subscriptions$ = new Array<Subscription>();

  constructor(
    private store: Store<CalendarState>,
    private fb: FormBuilder,
    private reminderService: RemindersService,
    private citiesService: CitiesService,
    private weatherService: WeatherService) { }

  ngOnInit() {
    this.fillProperties();
    this.store.pipe(select(choosenDate)).subscribe(date => this.date = date);
    this.store.pipe(select(getReminders)).subscribe(reminders => this.buildAgenda(reminders));
  }

  fillProperties() {
    this.status = 'view';
    this.daysNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    this.initialColor = '#abcdef';
    this.color = this.initialColor;
    this.form.get('color').setValue(this.initialColor);
    this.placesOptions = { types: ['(cities)'] };
    this.cityFieldValue = this.form.controls.city.value ? `${this.form.controls.city.value}, ${this.form.controls.country.value}` : '';
    this.fillButtonIcons();
  }

  fillButtonIcons() {
    this.newReminderIcon = faPlus;
    this.saveReminderIcon = faCheck;
    this.cancelReminderIcon = faTimes;
  }

  buildAgenda(reminders: Array<Reminder>) {
    this.unsubscribe();
    this.reminders = reminders;
    this.weatherIcons = new Array<any>();
    reminders.forEach(reminder => {
      const city = `${reminder.city.replace(/s/g, '+')},${reminder.country}`;
      this.subscriptions$.push(this.weatherService.get(city.toLowerCase()).subscribe(forecast => {
        const selected = this.selectClosestTime(forecast, reminder.datetime);
        this.weatherIcons.push({
          icon: get(selected, 'weather[0].icon', '50n'),
          alt: get(selected, 'weather[0].description', 'mist')
        });
      }));
    });
  }

  getDayName() {
    if (this.date) {
      return this.daysNames[this.date.day()];
    }
    return 'Sun';
  }

  toogleNewReminder() {
    this.status = 'create';
  }

  cancel() {
    this.status = 'view';
    this.form.reset();
  }

  save() {
    if (this.form.valid) {
      this.status = 'view';
      this.store.dispatch(new AddReminder(this.reminderService.parse(this.form.value, this.date)));
      this.form.reset();
      this.color = this.initialColor;
      this.form.get('color').setValue(this.initialColor);
    } else {
      Object.keys(this.form.controls).forEach(key => {
        const errors = this.form.controls[key].errors;
        if (errors) {
          Object.keys(errors).forEach(error => console.log(`${key}: ${error} (current value: ${this.form.controls[key].value})`));
        }
      });
    }
  }

  pickedColor(newColor: string) {
    this.form.controls.color.setValue(newColor);
  }

  pickedCity(input: any) {
    const parsed = this.citiesService.parse(input);
    this.form.patchValue(parsed);
  }

  unsubscribe() {
    this.subscriptions$.forEach(subs => subs.unsubscribe());
    this.subscriptions$ = new Array<Subscription>();
  }

  selectClosestTime(forecast: any, goal: moment.Moment) {
    let closestDifference = Number.MAX_SAFE_INTEGER;
    let selectedTime = forecast[0].time;
    forecast.forEach(prediction => {
      const difference = Math.abs(prediction.time.diff(goal, 'hours'));
      if (difference < closestDifference) {
        selectedTime = prediction.time;
        closestDifference = difference;
      }
    });
    return forecast.find(prediction => prediction.time.diff(selectedTime) === 0);
  }
}
