import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalendarDateComponent } from './components/calendar-date/calendar-date.component';
import { CalendarHeaderComponent } from './components/calendar-header/calendar-header.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { CalendarWeekComponent } from './components/calendar-week/calendar-week.component';
import { CalendarStoreModule } from './store/calendar-store.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ListRemindersComponent } from './components/list-reminders/list-reminders.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { ColorPickerModule } from 'ngx-color-picker';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

@NgModule({
  declarations: [
    AppComponent,
    CalendarDateComponent,
    CalendarHeaderComponent,
    CalendarComponent,
    CalendarWeekComponent,
    ListRemindersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarStoreModule,
    FontAwesomeModule,
    NgxMaterialTimepickerModule,
    BrowserAnimationsModule,
    ColorPickerModule,
    GooglePlaceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
