import * as moment from 'moment';

export function resetTime(date: moment.Moment | Date): moment.Moment {
    if (date instanceof Date) {
        date = parseToMoment(date);
    }
    date.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    return date;
}

export function parseToMoment(date: Date): moment.Moment {
    return moment(date);
}
