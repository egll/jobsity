export interface City {
    [id: number]: string;
    name: string;
    country: string;
}
