import { Reminder } from './reminder.interface';

export interface CalendarDate {
    date: string;
    isSelected: boolean;
    isToday: boolean;
    isCurrentMonth: boolean;
    reminders: Array<Reminder>;
}

export class CalendarDateImpl implements CalendarDate {
    date: string;
    isSelected: boolean;
    isToday: boolean;
    isCurrentMonth: boolean;
    reminders: Array<Reminder>;

    constructor(
        date: string,
        isSelected: boolean = false,
        isToday: boolean = false,
        isCurrentMonth: boolean = false,
        reminders?: Array<Reminder>
    ) {
        this.date = date;
        this.isSelected = isSelected;
        this.isToday = isToday;
        this.isCurrentMonth = isCurrentMonth;
        if (!reminders) {
            this.reminders = new Array<Reminder>();
        }
    }
}
