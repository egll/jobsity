import * as moment from 'moment';

export interface Reminder {
    id: string;
    name: string;
    datetime: moment.Moment;
    city: string;
    country: string;
    color: string;
}

