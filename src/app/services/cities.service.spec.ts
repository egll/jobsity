import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CitiesService } from './cities.service';

describe('CitiesService', () => {
  let service: CitiesService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        CitiesService
      ],
    });

    service = TestBed.inject(CitiesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
