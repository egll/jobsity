import { Injectable } from '@angular/core';
import { Reminder } from '../models/reminder.interface';
import * as moment from 'moment';
import * as uuid from 'uuid/v4';

@Injectable({
  providedIn: 'root'
})
export class RemindersService {

  constructor() { }

  public parse(json: any, date: moment.Moment): Reminder {
    const time = json.time.split(':');
    const reminderDateTime = date.clone().set({hour: time[0], minute: time[1]});
    return {
      id: uuid(),
      name: json.title,
      datetime: reminderDateTime,
      city: json.city,
      country: json.country,
      color: json.color
    };
  }
}
