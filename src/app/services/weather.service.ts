import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  public get(city: string) {
    return this.http.get(`http://api.openweathermap.org/data/2.5/forecast?q=${city}&APPID=${environment.openWeatherApi}`)
      .pipe(map((res: any) => {
        return res.list.map(weather => {
          return {
            time: moment(weather.dt * 1000, 'x'),
            weather: weather.weather
          };
        });
      }));
  }
}
