import { Injectable } from '@angular/core';
import { City } from '../models/city.interface';
import { get } from 'lodash';


@Injectable({
  providedIn: 'root'
})
export class CitiesService {
  cities: City[];

  constructor() { }

  parse(gPlacesResponse: any) {
    const city = get(gPlacesResponse, 'name', '');
    const countryComponents = gPlacesResponse.address_components.filter(component => component.types.find(type => type === 'country'))
      || [];
    const country = get(countryComponents, '[0].short_name', '');
    return {
      city,
      country
    };
  }
}
