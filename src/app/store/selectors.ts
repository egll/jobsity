import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CalendarState } from './reducers';
import * as moment from 'moment';


const getCalendarState = createFeatureSelector<CalendarState>('calendar');

export const choosenDate = createSelector(getCalendarState, (state: CalendarState) => {
    return state.selected;
});

export const cleanStatus = createSelector(getCalendarState, (state: CalendarState) => {
    return state.isClean;
});

export const getReminders = createSelector(getCalendarState, (state: CalendarState) => {
    return state.reminders
        .filter(reminder => state.selected.isSame(reminder.datetime, 'days'))
        .sort((a, b) => a.datetime.diff(b.datetime));
});
