import * as moment from 'moment';
import { Reminder } from '../models/reminder.interface';
import { resetTime } from '../utils';
import { CalendarActions, ActionTypes } from './actions';

export interface CalendarState {
    isClean: boolean;
    selected: moment.Moment;
    reminders: Array<Reminder>;
}

const initialState: CalendarState = {
    isClean: true,
    selected: resetTime(moment()),
    reminders: new Array<Reminder>()
};

export function reducer(state = initialState, action: CalendarActions): CalendarState {
    switch (action.type) {
        case ActionTypes.SetSelectedDate: {
            return {
                ...state,
                selected: action.payload,
                isClean: false
            };
        }
        case ActionTypes.AddReminder: {
            const reminders = state.reminders;
            reminders.push(action.payload);
            return {
                ...state,
                reminders
            };
        }
        default:
            return state;
    }
}
