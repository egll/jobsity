import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromCalendar from './reducers';

export interface State {
  calendar: fromCalendar.CalendarState;
}

export const reducers: ActionReducerMap<State> = {
  calendar: fromCalendar.reducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
