import { Action } from '@ngrx/store';
import * as moment from 'moment';
import { Reminder } from '../models/reminder.interface';

export enum ActionTypes {
    SetSelectedDate = '[Calendar] Select Date',
    AddReminder = '[Reminder] Add'
}

export class SetSelectedDate implements Action {
    public readonly type = ActionTypes.SetSelectedDate;

    constructor(public payload: moment.Moment) { }
}

export class AddReminder implements Action {
    public readonly type = ActionTypes.AddReminder;

    constructor(public payload: Reminder) { }
}

export type CalendarActions = SetSelectedDate | AddReminder;
