export const environment = {
  production: true,
  openWeatherApi: 'a7cc3e46c47f18efdedc24e39f7ecbf5',
  shortDateFormat: 'YYYY-MM-DD'
};
