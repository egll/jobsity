# Jobsity Challenge: Calendar

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.0-rc.9.

## Packages used

1. [Angular v.9](https://next.angular.io/): Framework used to develop the front-end.
1. [ngRx](https://ngrx.io/): State manager framework.
1. [Bootstrap 4](https://getbootstrap.com/): Responsive styles and grid.
1. [Font Awesome 5](https://fontawesome.com/): Icons for buttons.
1. [Places API Autocomplete](https://developers.google.com/places/web-service/autocomplete): Library used to autocomplete city names and countries.

## Goals

- [x] Rendering single month calendar.

> Done. It can be seen at the initial page, although I have modified the styles.

- [x] Ability to add a new "reminder" (max 30 chars) for a user entered day and time. Also, include a city.

> Select a date and click the '+' symbol to add a new remider for that date.

- [x] Display reminders on the calendar view in the correct time order.

> Properly showing all the reminders in the right portion of the screen.

- [x] Allow the user to select color when creating a reminder and display it appropriately.

> This is possible when adding a new reminder. The color is shown as the background color of the reminder.

- [ ] Ability to edit reminders – including changing text, city, day, time and color.

> Missed. It could be done by populating the form once the user clicks the respective reminder.

- [x] Add a weather service call from a free API such as ​ Open Weather Map​ , and get the weather forecast (ex. Rain) for the date of the calendar reminder based on the city.

> It shows the icon of the weather forecast for that hour at the end of every remider. Due to limitations of the 'free' tier service in Open Weather Map, the forecast only shows future hours.

- [ ] Unit test the functionality: ​ Ability to add a new "reminder" (max 30 chars) for a user entered day and time. Also, include a city.

> Missed. Needed more time to develop unit tests.
  
### Bonus

- [x] Expand the calendar to support more than the current month.

> When the user selects a date outside of the month, it will automatically switch to that month. It's possible to add a better UI and functionality to switch months easier.

- [x] Properly handle overflow when multiple reminders appear on the same date.

> The reminders appear as a list, considering that only have initial time, not duration.

- [ ] Functionality to delete one or ALL the reminders for a specific day.

> Missing. Needed more time to implement this functionality.

## Development server

1. Install all the packages using `npm i`.
1. Run `npm start` for a dev server.
1. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
